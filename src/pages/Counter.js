import React, { Component } from 'react'

export default class extends Component {
    constructor(props) {
        super(props)
        this.state = {
            counter: 0
        };
    }
    // handleIncrease = {
    //     this.setState({
    //         counter: this.state.counter + 1
    //     })
    // }
  render() { 
    return (
      <div>
        <h1 className="text-center">{this.state.counter}</h1>
        <div className="text-center">
            <button className='btn btn-warning me-2' 
            onClick={() => {
                this.setState({
                    counter: this.state.counter + 1,
                })
            }}
            >
            Increase
            </button>
            <button className="btn btn-danger"
            
            disabled ={this.state.counter > 0? false : true}

            onClick ={() => {
                this.setState({
                    counter: this.state.counter - 1,
                })
            }}

            >Descrease</button>
        </div>
      </div>
    // <div className='container'>
    //     <h2 className="text-center">{this.state.counter}</h2>
    //     <div className="text-center">
    //         <button className='btn btn-warning me-2' 
    //         onClick={() => {
    //             this.setState({
    //                 counter: this.state.counter + 1,
    //             })
    //         }}
    //         >
    //         Increase
    //         </button>
    //         <button className='btn btn-danger'
            
    //         disabled = {this.state.counter === 0 ? true :false}

    //         onClick={() => {
    //             this.setState({
    //                 counter: this.state.counter - 1,
    //                 // counter: (this.state.counter > 0)? this.state.counter - 1: 0
    //             })
    //         }}
    //         >Descrease
    //         </button>
    //     </div>
    //   </div>
    )
  }
}
